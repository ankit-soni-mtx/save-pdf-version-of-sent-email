/*
 * CreatedBy - ankitSoni 
 */
public class EmailAttachmentTriggerHandler {
    
    public static void generatePdfofEmail(List<EmailMessage> emailMessageList) {
        String emailHTMLBody = '';
        String recordId;

        for(EmailMessage em : emailMessageList){

            if(em.HtmlBody != null){
                emailHTMLBody = em.HtmlBody;
            }
            if(em.RelatedToId != null){
                recordId = em.RelatedToId;
            }
        }

        AttachEmailPDF.generateEmailPDF(recordId, emailHTMLBody);
    }
}
