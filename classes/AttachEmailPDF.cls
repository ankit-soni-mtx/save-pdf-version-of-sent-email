/*
 * CreatedBy - ankitSoni 
 */
public class AttachEmailPDF{
   
    public static final String FORM_HTML_START = '<HTML><BODY>';
    public static final String FORM_HTML_END = '</BODY></HTML>';

    public static void generateEmailPDF( String recordId , String htmlBody){
        String pdfContent;
        try{
            pdfContent = htmlBody;
        }catch(Exception e){
            pdfContent = '' + FORM_HTML_START;
            pdfContent = pdfContent + '<p>THERE WAS AN ERROR GENERATING EMAIL PDF: ' + e.getMessage() + '</p>';
            pdfContent = pdfContent + FORM_HTML_END;
        }
        attachPDF(recordId , pdfContent);
    }
   
    public static void attachPDF(String recordId, String pdfContent){
        System.debug('attachPDF>>>');

        try{
            Attachment attachmentPDF = new Attachment();
            attachmentPDF.parentId = recordId;
            attachmentPDF.Name = recordId+'Email' + '.pdf';
            attachmentPDF.body = Blob.toPDF(pdfContent); //This creates the PDF content
            insert attachmentPDF;
        }catch(Exception e){
            System.debug('Attachment Insert Exception - '+e.getMessage());
        }
    }
   
}