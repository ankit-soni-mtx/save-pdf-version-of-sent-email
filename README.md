## Asset Description :
- This asset contains 1 Apex Trigger and 2 Apex Classes.
- `AttachEmailPDF` apex class contains the logic which saves the Email body in a PDF Version and saves to the related record.
- `EmailAttachmentTriggerHandler` is the handler apex class for the `EmailAttachment Trigger` where the email body is passed to the above apex class.
- `EmailAttachment Trigger` is a apex trigger on `EmailMessage` object .
- Record of EmailMessage object is created everytime an email is been sent via apex , it saves the every Email Field used in the email.