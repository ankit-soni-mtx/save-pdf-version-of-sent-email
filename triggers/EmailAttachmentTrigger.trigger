trigger EmailAttachmentTrigger on EmailMessage (after insert) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){
            EmailAttachmentTriggerHandler.generatePdfofEmail(trigger.new);
        }
    }
}